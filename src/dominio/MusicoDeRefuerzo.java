package dominio;

public class MusicoDeRefuerzo extends PersonaDeLaBanda {
	private String instrumento;
	private int ingreso;

	public MusicoDeRefuerzo(String nombre, String apellido1, String apellido2, String instrumento, int ingreso) {
		super(nombre, apellido1, apellido2)
		this.instrumento = instrumento;
		this.ingreso = ingreso;
	}

	public String generarCadenaParaMostrar() {
		return this.nombre + " " this.apellido1 + " " + this.apellido2 + " ," + this.instrumento + ", recibe: " + this.ingreso + " €";
	}

	public String toString() {
		return "MusicoDeRefuerzo: " + this.nombre + " " + this.apellido1 + " " + this.apellido2 + " Instrumento: " + this.instrumento + " Ingreso: " + this.ingreso + " €";
	}
}
