package dominio;

public abstract class PersonaDeLaBanda {
	protected String nombre;
	protected String apellido1;
	protected String apellido2;

	public PersonaDeLaBanda(String nombre, String apellido1, String apellido2) {
		this.nombre = nombre;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
	}

	public abstract String generarCadenaParaMostrar();
}
