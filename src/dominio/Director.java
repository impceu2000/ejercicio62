package dominio;

public class Director extends PersonaDeLaBanda {

	public Director(String nombre, String apellido1, String apellido2) {
		super (nombre, apellido1, apellido2);
	}

	public String generarCadenaParaMostrar() {
		return this.nombre + " " + this.apellido1 + " " + this.apellido2 + ", director \n";
	}

	public String toString() {
		return "Directror: " + this.nombre + " " + this.apellido1 + " " + this.apellido2 + "\n";
	}
}
