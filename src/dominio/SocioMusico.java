package dominio;

public class SocioMusico extends PersonaDeLaBanda {
	private String instrumento;
	private int numeroDeSocio;

	public SocioMusico(String nombre, String apellido1, String apellido2, String instrumento, int numeroDeSocio) {
		super(nombre, apellido1, apellido2);
		this.instrumento = instrumento;
		this.numeroDeSocio = numeroDeSocio;
	}

	public String generarCadenaParaMostrar() {
		return this.nombre + " " +  this.apellido1 + " " + this.apellido2 + ", " + this.instrumento + ", número de socio: " + this.numeroDeSocio + "\n";
	}

	public String toString() {
		return "SocioMusico: " + this.nombre + " " +  this.apellido1 + " " + this.apellido2 + " Instrumento: " + this.instrumento + " Numero de socio: " + this.numeroDeSocio + "\n";
	}
}
